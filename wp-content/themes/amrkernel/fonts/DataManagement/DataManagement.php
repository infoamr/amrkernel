<?php

$set = strtolower( basename(__FILE__, '.php') );

$$set = array(
	'website (DataManagement) ' => $set . '_e900',
	'website1 (DataManagement) ' => $set . '_e901',
	'server (DataManagement) ' => $set . '_e902',
	'server1 (DataManagement) ' => $set . '_e903',
	'server2 (DataManagement) ' => $set . '_e904',
	'protection (DataManagement) ' => $set . '_e905',
	'process (DataManagement) ' => $set . '_e906',
	'password (DataManagement) ' => $set . '_e907',
	'network (DataManagement) ' => $set . '_e908',
	'maintenance (DataManagement) ' => $set . '_e909',
	'investigate (DataManagement) ' => $set . '_e90a',
	'internet (DataManagement) ' => $set . '_e90b',
	'hacker (DataManagement) ' => $set . '_e90c',
	'firewall (DataManagement) ' => $set . '_e90d',
	'filtering (DataManagement) ' => $set . '_e90e',
	'file (DataManagement) ' => $set . '_e90f',
	'file-directory (DataManagement) ' => $set . '_e910',
	'error (DataManagement) ' => $set . '_e911',
	'delete (DataManagement) ' => $set . '_e912',
	'data (DataManagement) ' => $set . '_e913',
	'database (DataManagement) ' => $set . '_e914',
	'data1 (DataManagement) ' => $set . '_e915',
	'data2 (DataManagement) ' => $set . '_e916',
	'connection (DataManagement) ' => $set . '_e917',
	'computer (DataManagement) ' => $set . '_e918',
	'coding (DataManagement) ' => $set . '_e919',
	'code (DataManagement) ' => $set . '_e91a',
	'cloud-storage (DataManagement) ' => $set . '_e91b',
	'cloud (DataManagement) ' => $set . '_e91c',
	'backup (DataManagement) ' => $set . '_e91d'
);