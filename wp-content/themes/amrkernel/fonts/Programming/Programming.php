<?php

$set = strtolower( basename(__FILE__, '.php') );

$$set = array(
	'coding (Programming) ' => $set . '_e900',
	'website (Programming) ' => $set . '_e901',
	'virus (Programming) ' => $set . '_e902',
	'website1 (Programming) ' => $set . '_e903',
	'hard-disk (Programming) ' => $set . '_e904',
	'server (Programming) ' => $set . '_e905',
	'search (Programming) ' => $set . '_e906',
	'script (Programming) ' => $set . '_e907',
	'programming (Programming) ' => $set . '_e908',
	'coding1 (Programming) ' => $set . '_e909',
	'password (Programming) ' => $set . '_e90a',
	'smartphone (Programming) ' => $set . '_e90b',
	'keyboard (Programming) ' => $set . '_e90c',
	'server1 (Programming) ' => $set . '_e90d',
	'anonymous (Programming) ' => $set . '_e90e',
	'website-design (Programming) ' => $set . '_e90f',
	'global (Programming) ' => $set . '_e910',
	'folder (Programming) ' => $set . '_e911',
	'error (Programming) ' => $set . '_e912',
	'diagram (Programming) ' => $set . '_e913',
	'chart (Programming) ' => $set . '_e914',
	'data-processing (Programming) ' => $set . '_e915',
	'earth (Programming) ' => $set . '_e916',
	'cpu (Programming) ' => $set . '_e917',
	'coding2 (Programming) ' => $set . '_e918',
	'binary-code (Programming) ' => $set . '_e919',
	'cloud-computing (Programming) ' => $set . '_e91a',
	'worldwide (Programming) ' => $set . '_e91b',
	'programming1 (Programming) ' => $set . '_e91c',
	'api (Programming) ' => $set . '_e91d'
);