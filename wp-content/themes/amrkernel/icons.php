<?php
$icons = sanitize_text_field('

@font-face{ font-family:"AgileMethodology";src:url("' . get_parent_theme_file_uri( 'fonts/AgileMethodology/AgileMethodology.woff' ) . '") format("woff"),url("' . get_parent_theme_file_uri( 'fonts/AgileMethodology/AgileMethodology.ttf' ) . '") format("truetype"); }
*[data-ico-agilemethodology]:before{ font-family:AgileMethodology;content:attr(data-ico-agilemethodology); }

@font-face{ font-family:"ArtificialIntelligence";src:url("' . get_parent_theme_file_uri( 'fonts/ArtificialIntelligence/ArtificialIntelligence.woff' ) . '") format("woff"),url("' . get_parent_theme_file_uri( 'fonts/ArtificialIntelligence/ArtificialIntelligence.ttf' ) . '") format("truetype"); }
*[data-ico-artificialintelligence]:before{ font-family:ArtificialIntelligence;content:attr(data-ico-artificialintelligence); }

@font-face{ font-family:"Business";src:url("' . get_parent_theme_file_uri( 'fonts/Business/Business.woff' ) . '") format("woff"),url("' . get_parent_theme_file_uri( 'fonts/Business/Business.ttf' ) . '") format("truetype"); }
*[data-ico-business]:before{ font-family:Business;content:attr(data-ico-business); }

@font-face{ font-family:"BusinessAnalytics";src:url("' . get_parent_theme_file_uri( 'fonts/BusinessAnalytics/BusinessAnalytics.woff' ) . '") format("woff"),url("' . get_parent_theme_file_uri( 'fonts/BusinessAnalytics/BusinessAnalytics.ttf' ) . '") format("truetype"); }
*[data-ico-businessanalytics]:before{ font-family:BusinessAnalytics;content:attr(data-ico-businessanalytics); }

@font-face{ font-family:"BusinessMotivation";src:url("' . get_parent_theme_file_uri( 'fonts/BusinessMotivation/BusinessMotivation.woff' ) . '") format("woff"),url("' . get_parent_theme_file_uri( 'fonts/BusinessMotivation/BusinessMotivation.ttf' ) . '") format("truetype"); }
*[data-ico-businessmotivation]:before{ font-family:BusinessMotivation;content:attr(data-ico-businessmotivation); }

@font-face{ font-family:"CloudTechnology";src:url("' . get_parent_theme_file_uri( 'fonts/CloudTechnology/CloudTechnology.woff' ) . '") format("woff"),url("' . get_parent_theme_file_uri( 'fonts/CloudTechnology/CloudTechnology.ttf' ) . '") format("truetype"); }
*[data-ico-cloudtechnology]:before{ font-family:CloudTechnology;content:attr(data-ico-cloudtechnology); }

@font-face{ font-family:"Communication";src:url("' . get_parent_theme_file_uri( 'fonts/Communication/Communication.woff' ) . '") format("woff"),url("' . get_parent_theme_file_uri( 'fonts/Communication/Communication.ttf' ) . '") format("truetype"); }
*[data-ico-communication]:before{ font-family:Communication;content:attr(data-ico-communication); }

@font-face{ font-family:"ComputerTechnology";src:url("' . get_parent_theme_file_uri( 'fonts/ComputerTechnology/ComputerTechnology.woff' ) . '") format("woff"),url("' . get_parent_theme_file_uri( 'fonts/ComputerTechnology/ComputerTechnology.ttf' ) . '") format("truetype"); }
*[data-ico-computertechnology]:before{ font-family:ComputerTechnology;content:attr(data-ico-computertechnology); }

@font-face{ font-family:"Construction";src:url("' . get_parent_theme_file_uri( 'fonts/Construction/Construction.woff' ) . '") format("woff"),url("' . get_parent_theme_file_uri( 'fonts/Construction/Construction.ttf' ) . '") format("truetype"); }
*[data-ico-construction]:before{ font-family:Construction;content:attr(data-ico-construction); }

@font-face{ font-family:"CustomerSupport";src:url("' . get_parent_theme_file_uri( 'fonts/CustomerSupport/CustomerSupport.woff' ) . '") format("woff"),url("' . get_parent_theme_file_uri( 'fonts/CustomerSupport/CustomerSupport.ttf' ) . '") format("truetype"); }
*[data-ico-customersupport]:before{ font-family:CustomerSupport;content:attr(data-ico-customersupport); }

@font-face{ font-family:"DataManagement";src:url("' . get_parent_theme_file_uri( 'fonts/DataManagement/DataManagement.woff' ) . '") format("woff"),url("' . get_parent_theme_file_uri( 'fonts/DataManagement/DataManagement.ttf' ) . '") format("truetype"); }
*[data-ico-datamanagement]:before{ font-family:DataManagement;content:attr(data-ico-datamanagement); }

@font-face{ font-family:"Design";src:url("' . get_parent_theme_file_uri( 'fonts/Design/Design.woff' ) . '") format("woff"),url("' . get_parent_theme_file_uri( 'fonts/Design/Design.ttf' ) . '") format("truetype"); }
*[data-ico-design]:before{ font-family:Design;content:attr(data-ico-design); }

@font-face{ font-family:"Development";src:url("' . get_parent_theme_file_uri( 'fonts/Development/Development.woff' ) . '") format("woff"),url("' . get_parent_theme_file_uri( 'fonts/Development/Development.ttf' ) . '") format("truetype"); }
*[data-ico-development]:before{ font-family:Development;content:attr(data-ico-development); }

@font-face{ font-family:"Essential";src:url("' . get_parent_theme_file_uri( 'fonts/Essential/Essential.woff' ) . '") format("woff"),url("' . get_parent_theme_file_uri( 'fonts/Essential/Essential.ttf' ) . '") format("truetype"); }
*[data-ico-essential]:before{ font-family:Essential;content:attr(data-ico-essential); }

@font-face{ font-family:"FontAwesome";src:url("' . get_parent_theme_file_uri( 'fonts/FontAwesome/FontAwesome.woff' ) . '") format("woff"),url("' . get_parent_theme_file_uri( 'fonts/FontAwesome/FontAwesome.ttf' ) . '") format("truetype"); }
*[data-ico-fontawesome]:before{ font-family:FontAwesome;content:attr(data-ico-fontawesome); }

@font-face{ font-family:"FontAwesome5Brands";src:url("' . get_parent_theme_file_uri( 'fonts/FontAwesome5Brands/FontAwesome5Brands.woff' ) . '") format("woff"),url("' . get_parent_theme_file_uri( 'fonts/FontAwesome5Brands/FontAwesome5Brands.ttf' ) . '") format("truetype"); }
*[data-ico-fontawesome5brands]:before{ font-family:FontAwesome5Brands;content:attr(data-ico-fontawesome5brands); }

@font-face{ font-family:"FontAwesome5Regular";src:url("' . get_parent_theme_file_uri( 'fonts/FontAwesome5Regular/FontAwesome5Regular.woff' ) . '") format("woff"),url("' . get_parent_theme_file_uri( 'fonts/FontAwesome5Regular/FontAwesome5Regular.ttf' ) . '") format("truetype"); }
*[data-ico-fontawesome5regular]:before{ font-family:FontAwesome5Regular;content:attr(data-ico-fontawesome5regular); }

@font-face{ font-family:"FontAwesome5Solid";src:url("' . get_parent_theme_file_uri( 'fonts/FontAwesome5Solid/FontAwesome5Solid.woff' ) . '") format("woff"),url("' . get_parent_theme_file_uri( 'fonts/FontAwesome5Solid/FontAwesome5Solid.ttf' ) . '") format("truetype"); }
*[data-ico-fontawesome5solid]:before{ font-family:FontAwesome5Solid;content:attr(data-ico-fontawesome5solid); }

@font-face{ font-family:"HelpAndSupport";src:url("' . get_parent_theme_file_uri( 'fonts/HelpAndSupport/HelpAndSupport.woff' ) . '") format("woff"),url("' . get_parent_theme_file_uri( 'fonts/HelpAndSupport/HelpAndSupport.ttf' ) . '") format("truetype"); }
*[data-ico-helpandsupport]:before{ font-family:HelpAndSupport;content:attr(data-ico-helpandsupport); }

@font-face{ font-family:"Icon7Stroke";src:url("' . get_parent_theme_file_uri( 'fonts/Icon7Stroke/Icon7Stroke.woff' ) . '") format("woff"),url("' . get_parent_theme_file_uri( 'fonts/Icon7Stroke/Icon7Stroke.ttf' ) . '") format("truetype"); }
*[data-ico-icon7stroke]:before{ font-family:Icon7Stroke;content:attr(data-ico-icon7stroke); }

@font-face{ font-family:"NavigationAndMaps";src:url("' . get_parent_theme_file_uri( 'fonts/NavigationAndMaps/NavigationAndMaps.woff' ) . '") format("woff"),url("' . get_parent_theme_file_uri( 'fonts/NavigationAndMaps/NavigationAndMaps.ttf' ) . '") format("truetype"); }
*[data-ico-navigationandmaps]:before{ font-family:NavigationAndMaps;content:attr(data-ico-navigationandmaps); }

@font-face{ font-family:"Productivity";src:url("' . get_parent_theme_file_uri( 'fonts/Productivity/Productivity.woff' ) . '") format("woff"),url("' . get_parent_theme_file_uri( 'fonts/Productivity/Productivity.ttf' ) . '") format("truetype"); }
*[data-ico-productivity]:before{ font-family:Productivity;content:attr(data-ico-productivity); }

@font-face{ font-family:"Programming";src:url("' . get_parent_theme_file_uri( 'fonts/Programming/Programming.woff' ) . '") format("woff"),url("' . get_parent_theme_file_uri( 'fonts/Programming/Programming.ttf' ) . '") format("truetype"); }
*[data-ico-programming]:before{ font-family:Programming;content:attr(data-ico-programming); }

@font-face{ font-family:"Science";src:url("' . get_parent_theme_file_uri( 'fonts/Science/Science.woff' ) . '") format("woff"),url("' . get_parent_theme_file_uri( 'fonts/Science/Science.ttf' ) . '") format("truetype"); }
*[data-ico-science]:before{ font-family:Science;content:attr(data-ico-science); }

@font-face{ font-family:"SocialMediaAndNetwork";src:url("' . get_parent_theme_file_uri( 'fonts/SocialMediaAndNetwork/SocialMediaAndNetwork.woff' ) . '") format("woff"),url("' . get_parent_theme_file_uri( 'fonts/SocialMediaAndNetwork/SocialMediaAndNetwork.ttf' ) . '") format("truetype"); }
*[data-ico-socialmediaandnetwork]:before{ font-family:SocialMediaAndNetwork;content:attr(data-ico-socialmediaandnetwork); }

@font-face{ font-family:"TeamWork2";src:url("' . get_parent_theme_file_uri( 'fonts/TeamWork2/TeamWork2.woff' ) . '") format("woff"),url("' . get_parent_theme_file_uri( 'fonts/TeamWork2/TeamWork2.ttf' ) . '") format("truetype"); }
*[data-ico-teamwork2]:before{ font-family:TeamWork2;content:attr(data-ico-teamwork2); }

@font-face{ font-family:"Teamwork";src:url("' . get_parent_theme_file_uri( 'fonts/Teamwork/Teamwork.woff' ) . '") format("woff"),url("' . get_parent_theme_file_uri( 'fonts/Teamwork/Teamwork.ttf' ) . '") format("truetype"); }
*[data-ico-teamwork]:before{ font-family:Teamwork;content:attr(data-ico-teamwork); }

@font-face{ font-family:"Transportation";src:url("' . get_parent_theme_file_uri( 'fonts/Transportation/Transportation.woff' ) . '") format("woff"),url("' . get_parent_theme_file_uri( 'fonts/Transportation/Transportation.ttf' ) . '") format("truetype"); }
*[data-ico-transportation]:before{ font-family:Transportation;content:attr(data-ico-transportation); }

@font-face{ font-family:"VirtualReality";src:url("' . get_parent_theme_file_uri( 'fonts/VirtualReality/VirtualReality.woff' ) . '") format("woff"),url("' . get_parent_theme_file_uri( 'fonts/VirtualReality/VirtualReality.ttf' ) . '") format("truetype"); }
*[data-ico-virtualreality]:before{ font-family:VirtualReality;content:attr(data-ico-virtualreality); }', array() );