<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'suninter_WP4AF');

/** MySQL database username */
define('DB_USER', 'suninter_WP4AF');

/** MySQL database password */
define('DB_PASSWORD', '*}m.a7zd_Qzr9Bs%y');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'ff87040f9e83b60c6f502016b507f3a15ea06fd7141c5dc6ead32e9ceb025a41');
define('SECURE_AUTH_KEY', 'eb6274f23025ea291b6b9c2dac73641a544d3efa3542cc16fbc935ed5485bad5');
define('LOGGED_IN_KEY', '076f54e63a370ae40ea87684ea80292c934ef6c918041efda10368c65ddb8fe0');
define('NONCE_KEY', 'fde8e0137d69914d36fd15369e36fd1805715d6511fa1ffe7088f15de674a305');
define('AUTH_SALT', '80c0ad930859fb033018d5fa98be65ff2c7ae22e4944d8c0e5c8d5ef960eaf25');
define('SECURE_AUTH_SALT', 'c921d0cbf025ad757fe3f49ad6b577239e93081fd7fb5e9401d52ca5c783d6aa');
define('LOGGED_IN_SALT', '43641cf0745dd460a0dcf03ce3a7ee924a2d05c94d5c34e2fcf4c9e89bd18829');
define('NONCE_SALT', '1622a585bfcb0ecc38c765cce3a5030e0c598faa6eb6bbc58b350de205456ae5');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'ywf_';
define('WP_CRON_LOCK_TIMEOUT', 120);
define('AUTOSAVE_INTERVAL', 300);
define('WP_POST_REVISIONS', 5);
define('EMPTY_TRASH_DAYS', 7);
define('WP_AUTO_UPDATE_CORE', true);

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
